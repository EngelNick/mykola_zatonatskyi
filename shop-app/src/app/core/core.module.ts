import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LandingComponent } from './landing/landing.component';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { BrandCardComponent } from './landing/brand-card/brand-card.component';
import { ShopPageComponent } from './shop-page/shop-page.component';
import { RouterModule } from '@angular/router';
import { ProductCardComponent } from './shop-page/product-card/product-card.component';

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    HttpClientModule,
    RouterModule,
  ],
  declarations: [
    LandingComponent,
    BrandCardComponent,
    ShopPageComponent,
    ProductCardComponent
  ],
})
export class CoreModule {
}
