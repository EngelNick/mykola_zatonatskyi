import { Component, OnInit } from '@angular/core';
import { ShopService } from '../shop.service';
import { BehaviorSubject } from 'rxjs';
import { Product } from '../../shared/models/product';

@Component({
  selector: 'app-shop-page',
  templateUrl: './shop-page.component.html',
  styleUrls: ['./shop-page.component.css']
})
export class ShopPageComponent implements OnInit {

  public products$ = new BehaviorSubject<Product[]>(null);

  constructor(private shopService: ShopService) {
  }

  ngOnInit() {
    this.shopService.getProducts()
      .subscribe();
    this.getBrands();
  }

  private getBrands() {
    this.products$ = this.shopService.products$;
  }

}
