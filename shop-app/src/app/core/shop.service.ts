import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { Brand } from '../shared/models/brand';
import { map, tap } from 'rxjs/operators';
import { Product } from '../shared/models/product';

@Injectable({
  providedIn: 'root'
})
export class ShopService {

  public brands$ = new BehaviorSubject<Brand[]>(null);
  public products$ = new BehaviorSubject<Product[]>(null);

  constructor(private http: HttpClient) {
  }

  public getBrandsList(): Observable<any> {
    return this.http.get('https://us-central1-epam-hiring-event.cloudfunctions.net/options')
      .pipe(
        map((response: { color: any, brands: Brand[] }) => response.brands),
        tap((brands: Brand[]) => this.brands$.next(brands)));
  }

  public getProducts(): Observable<any> {
    return this.http.get('https://us-central1-epam-hiring-event.cloudfunctions.net/list')
      .pipe(
        tap((products: Product[]) => this.products$.next(products)));
  }
}
