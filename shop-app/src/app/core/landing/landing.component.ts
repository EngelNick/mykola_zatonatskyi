import { Component, OnInit } from '@angular/core';
import { ShopService } from '../shop.service';
import { BehaviorSubject } from 'rxjs';
import { Brand } from '../../shared/models/brand';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.css']
})
export class LandingComponent implements OnInit {

  public brands$ = new BehaviorSubject<Brand[]>(null);

  constructor(private shopService: ShopService) {
  }

  ngOnInit() {
    this.getBrands();
  }

  private getBrands() {
    this.brands$ = this.shopService.brands$;
  }

}
