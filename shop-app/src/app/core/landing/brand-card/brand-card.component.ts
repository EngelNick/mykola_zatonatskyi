import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { Brand } from '../../../shared/models/brand';

@Component({
  selector: 'app-brand-card',
  templateUrl: './brand-card.component.html',
  styleUrls: ['./brand-card.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BrandCardComponent implements OnInit {

  @Input() brand: Brand;

  constructor() {
  }

  ngOnInit() {
  }

}
