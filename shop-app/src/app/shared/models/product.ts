export class Product {
  public brand: string;
  public color: string;
  public index: number;
  public name: string;
  public picture: string;
  public price: number;
  public rating: number;
  public sale: boolean;
}
