import { Routes } from '@angular/router';
import { LandingComponent } from './core/landing/landing.component';
import { ShopPageComponent } from './core/shop-page/shop-page.component';

export const appRoutes: Routes = [
  {path: '', pathMatch: 'full', component: LandingComponent},
  {path: 'shop', component: ShopPageComponent},
  {path: 'shop/:brand', component: ShopPageComponent},
];
